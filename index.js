// EX1
document.getElementById("clickShow1").onclick = function () {
  // Input
  //Let 3 integers value that inputed
  var numb1 = parseInt(document.getElementById("num1").value);
  var numb2 = parseInt(document.getElementById("num2").value);
  var numb3 = parseInt(document.getElementById("num3").value);

  //Declare output variable
  var result = "";

  //Process
  if (numb1 < numb2 && numb1 < numb3) {
    if (numb2 < numb3) {
      result = `<p><i class="fa fa-caret-right"></i> The ascending order: ${numb1} < ${numb2} < ${numb3} </p>`;
    } else if (numb2 > numb3) {
      result = `<p><i class="fa fa-caret-right"></i> The ascending order: ${numb1} < ${numb3} < ${numb2} </p>`;
    }
  } else if (numb2 < numb3 && numb2 < numb1) {
    if (numb3 < numb1) {
      result = `<p><i class="fa fa-caret-right"></i> The ascending order: ${numb2} < ${numb3} < ${numb1} </p>`;
    } else if (numb3 > numb1) {
      result = `<p><i class="fa fa-caret-right"></i> The ascending order: ${numb2} < ${numb1} < ${numb3} </p>`;
    }
  } else if (numb3 < numb1 && numb3 < numb2) {
    if (numb1 < numb2) {
      result = `<p><i class="fa fa-caret-right"></i> The ascending order: ${numb3} < ${numb1} < ${numb2} </p>`;
    } else if (numb1 > numb2) {
      result = `<p><i class="fa fa-caret-right"></i> The ascending order: ${numb3} < ${numb2} < ${numb1} </p>`;
    }
  }
  //Show the result on the screen
  document.getElementById("resultBT1").innerHTML = result;
};

//======================================== START EX 2 ===============================================

// EX2
document.getElementById("clickShow2").onclick = function () {
  // Let value is inputed
  var user = document.getElementById("Greeting").value;

  //Declare output
  var name = "";
  //Process
  //way 1
  // if (user == "M") {
  //   name = `<p><i class="fa fa-caret-right"></i> Welcome to Mother </p>`;
  // } else if (user == "B") {
  //   name = `<p><i class="fa fa-caret-right"></i> Welcome to Father </p>`;
  // } else if (user == "A") {
  //   name = `<p><i class="fa fa-caret-right"></i> Welcome to Brother </p>`;
  // } else if (user == "S") {
  //   name = `<p><i class="fa fa-caret-right"></i> Welcome to Sister </p>`;
  // } else if (user == "E") {
  //   name = `<p><i class="fa fa-caret-right"></i> Welcome to Baby sister </p>`;
  // }

  //way 2
  switch (user) {
    case "M":
      name = `<p><i class="fa fa-caret-right"></i> Welcome to Mother </p>`;
      break;
    case "B":
      name = `<p><i class="fa fa-caret-right"></i> Welcome to Father </p>`;
      break;
    case "A":
      name = `<p><i class="fa fa-caret-right"></i> Welcome to Brother </p>`;
      break;
    case "S":
      name = `<p><i class="fa fa-caret-right"></i> Welcome to Sister </p>`;
      break;
    case "E":
      name = `<p><i class="fa fa-caret-right"></i> Welcome to Baby sister </p>`;
      break;
  }

  //Show the result on the screen
  document.getElementById("resultBT2").innerHTML = name;
};

//======================================== START EX 3 ===============================================

// EX3
document.getElementById("clickShow3").onclick = function () {
  // Let 3 integers value that inputed
  var n1 = parseInt(document.getElementById("BT3_num1").value);
  var n2 = parseInt(document.getElementById("BT3_num2").value);
  var n3 = parseInt(document.getElementById("BT3_num3").value);

  // Declare tempotary variable even integer and old integer
  var even = 0;
  var odd = 0;

  // Process
  n1 % 2 == 0 ? even++ : odd++;
  n2 % 2 == 0 ? even++ : odd++;
  n3 % 2 == 0 ? even++ : odd++;

  //Show the result on the screen
  var result_BT3 = `<p><i class="fa fa-caret-right"></i> Even integer: ${even} and Odd integer: ${odd} </p>`;
  document.getElementById("resultBT3").innerHTML = result_BT3;
};

//======================================== START EX 4 ===============================================

// EX 4
document.getElementById("clickShow4").onclick = function () {
  // Let 3 integers value that inputed
  var edge1 = parseInt(document.getElementById("inputEdge1").value);
  var edge2 = parseInt(document.getElementById("inputEdge2").value);
  var edge3 = parseInt(document.getElementById("inputEdge3").value);

  // Declare Pytago
  var pytago1 = Math.sqrt(edge1 ** 2 + edge2 ** 2);
  var pytago2 = Math.sqrt(edge1 ** 2 + edge3 ** 2);
  var pytago3 = Math.sqrt(edge2 ** 2 + edge3 ** 2);

  // Declare output
  var resultBT4 = "";

  // Process
  if (
    (edge1 == edge2 && edge1 != edge3) ||
    (edge1 == edge3 && edge1 != edge2) ||
    (edge2 == edge3 && edge1 != edge3)
  ) {
    resultBT4 = `<p><i class="fa fa-caret-right"></i> This is an Isosceles triangle </p>`;
  } else if (edge1 == edge2 && edge1 == edge3) {
    resultBT4 = `<p><i class="fa fa-caret-right"></i> This is an Equilateral triangle </p>`;
  } else if (pytago1 == edge3 || pytago2 == edge2 || pytago3 == edge1) {
    resultBT4 = `<p><i class="fa fa-caret-right"></i> This is a Square triangle (Pytago principle) </p>`;
  }

  //Show the result on the screen
  document.getElementById("result_BT4").innerHTML = resultBT4;
};

//========================== START EX 5 =======================
// EX 5
// Show result when user click yesterday button
document.getElementById("clickShow5_1").onclick = function () {
  // INPUT
  // Let value of variables inputed
  var valueDay = parseInt(document.getElementById("dayValue").value);
  var valueMonth = parseInt(document.getElementById("monthValue").value);
  var valueYear = parseInt(document.getElementById("yearValue").value);

  // OUTPUT
  // Call yesterday function
  yesterday(valueDay, valueMonth, valueYear);
};

// Show result when user click tomorrow button
document.getElementById("clickShow5_2").onclick = function () {
  // INPUT
  // Let value of variables inputed
  var valueDay = parseInt(document.getElementById("dayValue").value);
  var valueMonth = parseInt(document.getElementById("monthValue").value);
  var valueYear = parseInt(document.getElementById("yearValue").value);

  // OUTPUT
  // Call tomorrow function
  tomorrow(valueDay, valueMonth, valueYear);
};

// PROCESS
// Identify leap year
function leapYear(year) {
  return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

// Identify months have 31 days and months have 30 days
function dateOfMonth(month, year) {
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12: {
      return 31;
    }
    case 2: {
      if (leapYear(year)) {
        return 29;
      }
      return 28;
    }
    case 4:
    case 6:
    case 9:
    case 11: {
      return 30;
    }
  }
}

// Calculate yesterday
function yesterday(d, m, y) {
  var last_y, last_d, last_m;
  last_y = y;
  last_m = m;
  last_d = d;

  last_d = d - 1;
  if (m != 1 && d == 1) {
    if (m == 2 || m == 4 || m == 6 || m == 8 || m == 9 || m == 11) {
      last_d = 31;
      last_m = m - 1;
    }
    if (m == 3) {
      if (leapYear(y)) {
        last_d = 29;
        last_m = m - 1;
      } else {
        last_d = 28;
        last_m = m - 1;
      }
    }
    if (m == 5 || m == 7 || m == 10 || m == 12) {
      last_d = 30;
      last_m = m - 1;
    }
  } else if (m == 1 && d == 1) {
    last_d = 31;
    last_y = y - 1;
    last_m = 12;
  }

  // OUTPUT
  // show result on the screen
  var result_1 = `<p><i class="fa fa-caret-right"></i> Yesterday: ${last_d}/${last_m}/${last_y} </p>`;
  document.getElementById("result_BT5").innerHTML = result_1;
}

// Calculate tomorrow
function tomorrow(d, m, y) {
  var next_y = y,
    next_m = m,
    next_d = d;

  next_d = d + 1;
  if (m != 12) {
    next_d = 1;
    next_m = m + 1;
  } else if (m == 12) {
    next_d = 1;
    next_y = y + 1;
    next_m = 1;
  } else if (m == 2) {
    if (leapYear(y)) {
      if (d == 29) {
        next_d = 1;
        next_m = m + 1;
      }
    } else {
      if (d == 28) {
        next_d = 1;
        next_m = m + 1;
      }
    }
  }

  // OUTPUT
  // show result on the screen
  var result_2 = `<p><i class="fa fa-caret-right"></i> Tomorrow: ${next_d}/${next_m}/${next_y} </p>`;
  document.getElementById("result_BT5").innerHTML = result_2;
}

//=================== START EX 6 =======================

// Ex 6
//PROCESS
// Identify leap year
function leapYear(year) {
  return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

// Identify months have 31 days and months have 30 days
function dateOfMonth(month, year) {
  var dayMonth = 0;
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      dayMonth = 31;
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      dayMonth = 30;
      break;
    case 2:
      if (leapYear(year)) {
        dayMonth = 29;
      } else {
        dayMonth = 28;
      }
      break;
  }
  // show result on the screen
  var resultBT6 = `<p><i class="fa fa-caret-right"></i> Tomorrow: ${dayMonth} </p>`;
  document.getElementById("result_BT6").innerHTML = resultBT6;
}

// OUTPUT
document.getElementById("clickShow6").onclick = function () {
  // Let value of variables inputed
  var valueMonth = parseInt(document.getElementById("monthValue_BT6").value);
  var valueYear = parseInt(document.getElementById("yearValue_BT6").value);

  // Call function
  dateOfMonth(valueMonth, valueYear);
};

//======================================== START EX 7 ===============================================

// Ex 7
document.getElementById("clickShow7").onclick = function () {
  // INPUT
  // Let value of variables inputed
  var valueNum7 = parseInt(document.getElementById("numBT7").value);

  // PROCESS
  if (valueNum7 < 100 || valueNum7 > 999) {
    alert("Number is invalid");
    return;
  } else {
    // Declare hundreds, dozens and units varibles
    var hundreds = Math.floor(valueNum7 / 100),
      dozens = Math.floor((valueNum7 / 10) % 10),
      units = valueNum7 % 10;

    //Print out dozens
    switch (hundreds) {
      case 1:
        hundreds = "Một trăm";
        break;
      case 2:
        hundreds = "Hai trăm";
        break;
      case 3:
        hundreds = "Ba trăm";
        break;
      case 4:
        hundreds = "Bốn trăm";
        break;
      case 5:
        hundreds = "Năm trăm";
        break;
      case 6:
        hundreds = "Sáu trăm";
        break;
      case 7:
        hundreds = "Bảy trăm";
        break;
      case 8:
        hundreds = "Tám trăm";
        break;
      case 9:
        hundreds = "Chín trăm";
        break;
    }

    //Print out dozens
    switch (dozens) {
      case 1:
        dozens = "mười";
        break;
      case 2:
        dozens = "hai mươi";
        break;
      case 3:
        dozens = "ba mươi";
        break;
      case 4:
        dozens = "bốn mươi";
        break;
      case 5:
        dozens = "năm mươi";
        break;
      case 6:
        dozens = "sáu mươi";
        break;
      case 7:
        dozens = "bảy mươi";
        break;
      case 8:
        dozens = "tám mươi";
        break;
      case 9:
        dozens = "chín mươi";
        break;
    }

    //Print out units
    switch (units) {
      case 1:
        units = "một";
        break;
      case 2:
        units = "hai";
        break;
      case 3:
        units = "ba";
        break;
      case 4:
        units = "bốn";
        break;
      case 5:
        units = "lăm";
        break;
      case 6:
        units = "sáu";
        break;
      case 7:
        units = "bảy";
        break;
      case 8:
        units = "tám";
        break;
      case 9:
        units = "chín";
        break;
    }
    if (dozens % 10 == 0 && units != 0) {
      dozens = "lẻ";
    } else if (dozens % 10 == 0 && units == 0) {
      dozens = "";
      units = "";
    }
  }

  // OUTPUT
  // show result on the screen
  var result_BT7 = `<p><i class="fa fa-caret-right"></i> ${hundreds} ${dozens} ${units}</p>`;
  document.getElementById("resultBT7").innerHTML = result_BT7;
};

//======================================== START EX 8 ===============================================

// EX 8

document.getElementById("clickShow8").onclick = function () {
  // INPUT
  // Get student'names which entered
  var STUDENT1 = document.getElementById("std1").value;
  var STUDENT2 = document.getElementById("std2").value;
  var STUDENT3 = document.getElementById("std3").value;

  // Get student'coordinates which entered
  // Student 1
  var x_std1 = document.getElementById("x-coord1").value * 1;
  var y_std1 = document.getElementById("y-coord1").value * 1;
  // Student 2
  var x_std2 = document.getElementById("x-coord2").value * 1;
  var y_std2 = document.getElementById("y-coord2").value * 1;
  // Student 3
  var x_std3 = document.getElementById("x-coord3").value * 1;
  var y_std3 = document.getElementById("y-coord3").value * 1;

  // Get university'coordinates which entered
  var x_univ = document.getElementById("x-coord-uni").value * 1;
  var y_univ = document.getElementById("y-coord-uni").value * 1;

  // PROCESS
  // call find distance function
  var dst1 = parseFloat(distanceStd(x_std1, y_std1, x_univ, y_univ));
  var dst2 = parseFloat(distanceStd(x_std2, y_std2, x_univ, y_univ));
  var dst3 = parseFloat(distanceStd(x_std3, y_std3, x_univ, y_univ));
  console.log("dst1: ", dst1);
  console.log("dst2: ", dst2);
  console.log("dst3: ", dst3);

  // call find MAX distance function
  var maxDst = maxDistance(dst1, dst2, dst3);
  console.log("Max distance: ", maxDst);

  var name = "";

  // Find studen'name who has furthest way
  if (maxDst == dst1) {
    name = STUDENT1;
  } else if (maxDst == dst2) {
    name = STUDENT2;
  } else if (maxDst == dst3) {
    name = STUDENT3;
  } else if (maxDst == 1) {
    name = "Equal";
  }
  // OUTPUT
  // show result on the screen

  console.log("name: ", name);

  var result_BT8 = `<p><i class="fa fa-caret-right"></i> The student who lives the furthest from the school: ${name} </p>`;
  document.getElementById("resultBT8").innerHTML = result_BT8;
};

// Calculate distance
function distanceStd(x1, y1, x2, y2) {
  var dist = parseFloat(Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2));
  return dist;
}

// Find MAX distance
function maxDistance(dist1, dist2, dist3) {
  if (dist1 < dist2 && dist1 < dist3) {
    if (dist2 > dist3) {
      return dist2;
    } else if (dist2 < dist3) {
      return dist3;
    }
  } else if (dist2 < dist1 && dist2 < dist3) {
    if (dist1 > dist3) {
      return dist1;
    } else if (dist1 < dist3) {
      return dist3;
    }
  } else if (dist3 < dist1 && dist3 < dist2) {
    if (dist1 > dist2) {
      return dist1;
    } else if (dist1 < dist2) {
      return dist2;
    }
  } else if (dist1 == dist2 && dist1 != dist3) {
    if (dist1 < dist3) {
      return dist3;
    } else if (dist1 > dist3) {
      return dist1;
    }
  } else if (dist1 == dist3 && dist1 != dist2) {
    if (dist1 < dist2) {
      return dist2;
    } else if (dist1 > dist2) {
      return dist1;
    }
  } else if (dist2 == dist3 && dist1 != dist2) {
    if (dist1 < dist2) {
      return dist2;
    } else if (dist1 > dist2) {
      return dist1;
    }
  } else if (dist2 == dist3 && dist1 == dist2) {
    return 1;
  }
}
